<?php

define("JAHR","2016");
define("BASIS_URL","http://frab.fossgis-konferenz.de");
define("CONF_URL","http://frab.fossgis-konferenz.de/en/foss4g-2016/public/schedule.json");
define("JSON_SPEAKERS","http://frab.fossgis-konferenz.de/en/foss4g-2016/public/speakers.json");
define("YT_URL","https://tracker.c3voc.de/api/v1/foss4g16/tickets/released.json");
define("YT_PLAYLIST","https://www.youtube.com/playlist?list=PLTli5-lbeoibm_kIG8rk-6XIffh6HfM7r");
//define("YT_URL","https://tracker.c3voc.de/api/v1/gpn16/tickets/released.json");		// eine andere Konferenz
//define("VID_URL","http://ftp5.gwdg.de/pub/misc/openstreetmap/FOSS4G2016/");
define("VID_URL","http://ftp5.gwdg.de/pub/misc/openstreetmap/FOSS4G-2016/");		// FOSS4G-2016
define("FRAB_EVENT_URL", "http://frab.fossgis-konferenz.de/en/foss4g-2016/public/events/");
define("FRAB_PERSON_URL","http://frab.fossgis-konferenz.de/en/foss4g-2016/public/speakers/");
define("LOGO_PATH","http://2016.foss4g.org/files/foss4g/layout/foss4g-logo.png");

define("ID_LEN", 6);
define("TIT_LEN", 60);
define("SUB_LEN", 60);
define("DESC_LEN", 200);
define("TIT_SHORT", 20);
define("SUB_SHORT", 20);
define("DESC_SHORT", 20);
define("PERS_LEN", 20);
define("YT_LEN", 11);
define("RAUM_LEN", 10);

define("HIDDEN_IDS", "||5105|");	// 5105 ist der Sektempfang
define("HIDDEN_ROOMS", "||HS432|HS3004|EXPO Forum|");
  // ||, damit die matches immer >0


?>
